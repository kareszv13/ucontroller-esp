#include <WiFiClient.h>
#include <ESP8266WiFi.h>

const char* ssid     = "your-ssid";
const char* password = "your-password";
const char* host = "http://espkrly.uw.hu/";

void setup() {
  Serial.begin(115200);
  delay(10);
  WiFi.begin(ssid, password);


  WiFiClient client;
  client._localPort = 123;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // We now create a URI for the request
  String url = "/index.php";


  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }

  Serial.println();
  Serial.println("closing connection");


}

void loop() {
  // put your main code here, to run repeatedly:

}
