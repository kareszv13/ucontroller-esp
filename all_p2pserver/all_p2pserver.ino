#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <Arduino.h>
#include <WiFiClient.h>

WiFiUDP Udp;
unsigned int localUdpPort = 4210;
char incomingPacket[255];
char  replyPacekt[] = "Hi there! Got the message :-)";
const char* host = "http://espkrly.uw.hu/";
ESP8266WiFiMulti WiFiMulti;


void setup() {
  if ((WiFiMulti.run() == WL_CONNECTED)) {}

  WiFiMulti.addAP("SSID", "PASSWORD");

  Udp.begin(localUdpPort);
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  Udp.write(replyPacekt);
  Udp.endPacket();


    WiFiClient client;
    client._localPort = 123;
const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
  
  // We now create a URI for the request
  String url = "/index.php";
 
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  
  Serial.println();
  Serial.println("closing connection");


}

void loop() {
  // put your main code here, to run repeatedly:

}
